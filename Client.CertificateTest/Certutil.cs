﻿using System;
using System.Collections.Generic;
using System.Text;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;

namespace Client.CertificateTest
{

    public class CertUtil
    {

        public static X509Certificate2 Load(string pfxFile, string password)
        {
            var assembly = typeof(CertUtil).Assembly;

            string[] resourceNames = assembly.GetManifestResourceNames(); 

            using (var stream = assembly.GetManifestResourceStream(pfxFile))
            {
                var file = Path.Combine(Path.GetTempPath(), "TestApp-" + Guid.NewGuid());
                //Logger.DebugFormat("Loading certificate from {0}", file);
                try
                {
                    File.WriteAllBytes(file, ReadStream(stream));
                    var cert = new X509Certificate2(file, password, X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.UserKeySet | X509KeyStorageFlags.Exportable);

                    Debug.WriteLine("Certificate Has Private Key: {0}", cert.HasPrivateKey);
                    //Console.WriteLine("HasCngKey: {0}", cert.HasCngKey());
                    Debug.WriteLine("Is Private Access Allowed: {0}", IsPrivateAccessAllowed(cert));

                    return cert;
                }
                finally
                {
                    File.Delete(file);
                }
            }
        }

        private static byte[] ReadStream(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static bool IsPrivateAccessAllowed(X509Certificate2 cert)
        {
            try
            {
                //if (cert.HasCngKey())
                //{
                //    var privateKey = cert.GetCngPrivateKey();
                //}
                //else
                //{
                    var privateKey = cert.PrivateKey;
                //}
                return true;
            }
            catch (CryptographicException ex)
            {
                //Logger.ErrorException("Error accessing private key", ex);
                return false;
            }
        }

    }
}
