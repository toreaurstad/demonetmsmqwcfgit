﻿Write-Host "Generating a self signed certificate for client in MSMQ WCF demo"

$clientCertFile = "C:\temp\MSMQWcfDemoClient.pfx" 

$clientCertFileRsaFormatted = "C:\temp\MSMQWcfDemoClient.RSAConverted.pfx"

$cert = New-SelfSignedCertificate -Subject "CN=MSMQWcfDemoClient" -certstorelocation cert:\localmachine\my `
-NotAfter (Get-Date).AddYears(3) -KeyLength 2048 -KeySpec KeyExchange
$pwd = ConvertTo-SecureString -String ‘bongo’ -Force -AsPlainText
$path = 'cert:\localMachine\my\' + $cert.thumbprint 
Export-PfxCertificate -cert $path -FilePath $clientCertFile -Password $pwd

$clientCertPasswordSec = ConvertTo-SecureString "bongo" -AsPlainText -Force


Write-Host "Generating a self signed certificate for server in MSMQ WCF demo"

$serverCertFile = "C:\temp\MSMQWcfDemoServer.pfx" 

$serverCertFileRsaFormatted =  "C:\temp\MSMQWcfDemoServer.RSAConverted.pfx"

$certServer = New-SelfSignedCertificate -Subject "CN=MSMQWcfDemoserver" -certstorelocation cert:\localmachine\my `
  -NotAfter (Get-Date).AddYears(3) -KeyExportPolicy Exportable -KeyLength 2048 -KeySpec KeyExchange
$pwdServer = ConvertTo-SecureString -String ‘kongo’ -Force -AsPlainText
$pathServer = 'cert:\localMachine\my\' + $certServer.thumbprint 
Export-PfxCertificate -cert $pathServer -FilePath $serverCertFile -Password $pwdServer

$serverCertPasswordSec = ConvertTo-SecureString "kongo" -AsPlainText -Force

$command = @'
cmd.exe /c xcopy convertcertificatetorsa.bat
cmd.exe /c c:\temp\convertcertificatetorsa.bat
'@

Write-Host "Starting bat file to convert from CNG to RSA format.." 

Invoke-Expression -Command:$command 

Write-Host "Importing RSA formatted certificates.."

Import-PfxCertificate -FilePath $clientCertFileRsaFormatted -CertStoreLocation Cert:\LocalMachine\My -Password $clientCertPasswordSec
Import-PfxCertificate -FilePath $clientCertFileRsaFormatted -CertStoreLocation Cert:\LocalMachine\root -Password $clientCertPasswordSec



Import-PfxCertificate -FilePath $serverCertFileRsaFormatted -CertStoreLocation Cert:\LocalMachine\My -Password $serverCertPasswordSec
Import-PfxCertificate -FilePath $serverCertFileRsaFormatted -CertStoreLocation Cert:\LocalMachine\root -Password $serverCertPasswordSec



<# Windows 7 users can try to use this extension to generate self signed certificates as makecert.exe is deprecated

. "$PSScriptRoot\New-SelfSignedCertificateEx.ps1"

$certificateServerFileName = "c:\temp\MSMQWcfDemoserver.pfx"
$certificateClientFileName = "c:\temp\MSMQWcfDemoclient.pfx"

Write-Host "Generation of WCF MSMQ Demo certificates" 


New-SelfSignedCertificateEx -Subject "CN=MSMQWcfDemoServer" -KeySpec Exchange `
 -KeyUsage "DataEncipherment, KeyEncipherment, DigitalSignature" -Path $certificateServerFileName `
  -Exportable -SAN "MyServer" -SignatureAlgorithm sha256  -AllowSMIME `
  -Password (ConvertTo-SecureString kongo -AsPlainText -Force)  -NotAfter (get-date).AddYears(3)

  
New-SelfSignedCertificateEx -Subject "CN=MSMQWcfDemoClient" -KeySpec Exchange `
 -KeyUsage "DataEncipherment, KeyEncipherment, DigitalSignature" -Path $certificateClientFileName `
  -Exportable -SAN "MyServer" -SignatureAlgorithm sha256  -AllowSMIME `
  -Password (ConvertTo-SecureString bongo -AsPlainText -Force)  -NotAfter (get-date).AddYears(3)

Write-Host "Certificate generation complete. Importing the pfx files for the MSMQ WCF Demo into LOCALMACHINE\MY Certificate store next.."
    
Get-ChildItem -Path "C:\temp" -Filter "MSMQWcf*.pfx" | Select Name 

Write-Host "Doing an automated certificate import. Type 'bongo' and then 'kongo' as the password" 

$serverCertPasswordSec = ConvertTo-SecureString "kongo" -AsPlainText -Force

$clientCertPasswordSec = ConvertTo-SecureString "bongo" -AsPlainText -Force


Import-PfxCertificate -FilePath $certificateClientFileName -CertStoreLocation Cert:\LocalMachine\My -Password $clientCertPasswordSec
Import-PfxCertificate -FilePath $certificateServerFileName -CertStoreLocation Cert:\LocalMachine\My -Password $serverCertPasswordSec

#>
