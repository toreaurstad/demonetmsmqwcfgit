﻿using System.Configuration;
using System.Diagnostics;
using System.Messaging;

namespace WcfDemoNetMsmqBinding.Host
{
    public class MessageQueueService : IMessageQueueService
    {

        public void SendMessage(string message)
        {
            //MessageQueueFactory.CreateMsmqIfMissing();
            string.Format("You entered: {0}", message);
        }

    }
}
