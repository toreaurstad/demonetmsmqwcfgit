﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WcfDemoNetMsmqBinding.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Demo of NetMsmqBinding in WCF");
            Console.WriteLine("Enter the message: ");
            var messageToMsmq = Console.ReadLine();
            MessageQueueServiceProxy.MessageQueueServiceClient client = new MessageQueueServiceProxy.MessageQueueServiceClient();
            client.SendMessage(messageToMsmq);

            Console.WriteLine("Demo complete. Hit Enter twice.");
            Console.ReadKey();
        }
    }
}
